const jwt = require('jsonwebtoken');

let users = {
  ['testEmail@gmail.com']: { password: "Password_1" },
}

exports.login = function (req, res) {

  let username = req.body.mail;
  let password = req.body.password;

  // Neither do this!
  if (!username || !password) {
    return res.status(401).send("Отказано в доступе")
  }

  //use the payload to store information about the user such as username, user role, etc.
  let payload = {username: username};

  //create the access token with the shorter lifespan
  let accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
    algorithm: "HS256",
    expiresIn: process.env.ACCESS_TOKEN_LIFE
  })

  //create the refresh token with the longer lifespan
  let refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
    algorithm: "HS256",
    expiresIn: process.env.REFRESH_TOKEN_LIFE
  })

  //store the refresh token in the user array
  if(!users[username]){
    return res.status(401).send("Неверный логин")
  }

  if(users[username].password !== password){
    return res.status(401).send("Неверный пароль")
  }

  users[username].refreshToken = refreshToken

  return res.send({
    token: accessToken
  })
}

exports.refresh = function (req, res){

  let accessToken = req.headers.authorization.split(' ')[1];

  if (!accessToken){
    return res.status(403).send();
  }

  let payload
  try{
    payload = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
  }
  catch(e){
    return res.status(401).send();
  }

  //retrieve the refresh token from the users array
  let refreshToken = users[payload.username].refreshToken;

  //verify the refresh token
  try{
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);
  }
  catch(e){
    return res.status(401).send();
  }

  let newToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET,
    {
      algorithm: "HS256",
      expiresIn: process.env.ACCESS_TOKEN_LIFE
    });

  return res.send({
    token: newToken
  })
}
