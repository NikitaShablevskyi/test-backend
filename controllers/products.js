const ProductDto = require('../models/product.dto');

const jwt = require('jsonwebtoken');

let products = [
  new ProductDto(1, 'Test product 1', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(2, 'Test product 2', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(3, 'Test product 3', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(4, 'Test product 4', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(5, 'Test product 5', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(6, 'Test product 6', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(7, 'Test product 7', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(8, 'Test product 8', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(9, 'Test product 9', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(10, 'Test product 9', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(11, 'Test product 10', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(12, 'Test product 11', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(13, 'Test product 12', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(14, 'Test product 13', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(15, 'Test product 14', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(16, 'Test product 15', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(17, 'Test product 16', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(18, 'Test product 17', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(19, 'Test product 18', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(20, 'Test product 19', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(21, 'Test product 20', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(22, 'Test product 21', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(23, 'Test product 23', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(24, 'Test product 24', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(25, 'Test product 25', 'Test description no matter long', 500, '/test/link'),
  new ProductDto(26, 'Test product 26', 'Test description no matter long', 500, '/test/link')
]

exports.getProductsInfo = (req, res) => {
  return res.status(200).send(products);
}

exports.getProductInfo = (req, res) => {
  let productId = Number(req.params.id);
  let productInfo = products.find(product => product.id === productId);
  if (productInfo) {
    return res.status(200).send(productInfo);
  }
  return res.status(400).send({
    message: "There are no such product"
  })
}

exports.createProduct = (req, res) => {
  const { name, description, price, imageLink } = req.body;
  if (!name || !description || !price || !imageLink) {
    return res.status(400).send({
      message: "Require full product info"
    })
  }

  const createdProduct = new ProductDto(products.length + 1, name, description, price, imageLink);
  products.push(createdProduct);
  return res.status(200).send({
    message: 'Successfully created'
  });
}

exports.updateProduct = (req, res) => {
  let productId = Number(req.params.id);
  const { name, description, price, imageLink } = req.body;
  if (!name || !description || !price || !imageLink) {
    return res.status(400).send({
      message: "Require full product info"
    })
  }
  let correspondentProductId = products.findIndex(product => product.id === productId);

  if (correspondentProductId == -1) {
    return res.status(400).send({
      message: "There are no such product"
    })
  }

  products[correspondentProductId] = new ProductDto(productId, name, description, price, imageLink);
  return res.status(200).send({
    message: 'Successfully updated'
  });
}

exports.deleteProduct = (req, res) => {
  let productId = Number(req.params.id);

  let correspondentProductId = products.findIndex(product => product.id = productId);

  if (correspondentProductId) {
    return res.status(400).send({
      message: "There are no such product"
    })
  }

  products.splice(correspondentProductId,1);
  return res.status(200).send({
    message: 'Successfully deleted'
  });
}
