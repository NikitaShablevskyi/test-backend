require('dotenv').config();
const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const path = require('path'); // модуль для парсинга пути
const cookieParser = require('cookie-parser');
const cors = require('cors')
const app = express()

const { login, refresh } = require('./controllers/authentication');
const { verify } = require('./controllers/middleware');
const {
  getProductsInfo,
  getProductInfo,
  createProduct,
  updateProduct,
  deleteProduct
} = require('./controllers/products')

app.use(cors());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(function (request, response, next) {

  let now = new Date();
  let hour = now.getHours();
  let minutes = now.getMinutes();
  let seconds = now.getSeconds();
  let data = `${hour}:${minutes}:${seconds} ${request.method} ${request.url} ${request.get("user-agent")}`;
  console.log(data);
  fs.appendFile("server.log", data + "\n", function () {});
  next();
});

app.get('/products', verify,getProductsInfo);
app.get('/products/:id', verify,getProductInfo);
app.post('/products', verify,createProduct);
app.put('/products/:id', verify,updateProduct);
app.delete('/products/:id', verify,deleteProduct);

app.post('/login', login);
app.post('/refrsh', refresh);

app.listen(3000);
