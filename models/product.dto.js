class ProductDto {
  id;
  name;
  description;
  price;
  imageLink;

  constructor(id, name, description, price, imageLink) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.imageLink = imageLink;
  }
}

module.exports = ProductDto;
